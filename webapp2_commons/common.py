"""
Utility layer on top of :class:`webapp2.RequestHandler`.

 - Basic session handling.
 - Rendering context processors can be defined and are executed on every
     request. These callables receive the request handler and must return a
     dictionary that will be added to the rendering context.

"""
import logging

from webapp2 import HTTPException
from webapp2 import RequestHandler
from webapp2 import WSGIApplication
from webapp2 import cached_property
from webapp2 import get_app
from webapp2 import get_request
from webapp2_extras import jinja2
from webapp2_extras import json
from webapp2_extras import sessions
from webapp2_extras.sessions import SessionStore
from webob import exc


__all__ = [
    'BaseRequestHandler',
    'RenderingContextProcessorsMiddleware',
    'add_url_utilities_to_rendering_context',
    ]


#{ Request handlers


class BaseRequestHandler(RequestHandler):

    def dispatch(self):
        self.session_store = sessions.get_store(request=self.request)
        try:
            super(BaseRequestHandler, self).dispatch()
        finally:
            self.session_store.save_sessions(self.response)

    @cached_property
    def session(self):
        return self.session_store.get_session()

    @cached_property
    def jinja2(self):
        """Return a Jinja2 renderer cached in the app registry."""
        return jinja2.get_jinja2(app=self.app)

    def get_default_rendering_context(self):
        default_rendering_context = {}

        rendering_context_processors = self.request.environ.get(
            RenderingContextProcessorsMiddleware.ENVIRON_KEY,
            [],
            )
        for processor in rendering_context_processors:
            default_rendering_context.update(processor(self))

        return default_rendering_context

    def render_response(self, template, **context):
        """Render a template and write the result to the response."""
        default_context = self.get_default_rendering_context()

        default_context.update(context)
        response = self.jinja2.render_template(template, **default_context)
        self.response.write(response)

    def json_response(self, data):
        """Return ``data`` as a JSON object to the client."""
        self.response.content_type = 'text/json'
        self.response.write(json.encode(data))


#{ Middleware


class WebAppMiddleware(WSGIApplication):

    def __init__(self, app, *args, **kwargs):
        WSGIApplication.__init__(self, *args, **kwargs)

        self.app = app

    def __call__(self, environ, start_response):
        with self.request_context_class(self, environ) as (request, response):
            environ['request'] = request
            environ['response'] = response

            try:
                if request.method not in self.allowed_methods:
                    # 501 Not Implemented.
                    raise exc.HTTPNotImplemented()
                return self.app(environ, start_response)

            except Exception as error:
                try:
                    # Try to handle it with a custom error handler.
                    self.handle_exception(request, response, error)
                except HTTPException, error:
                    # Use the HTTP exception as response.
                    response = error
                except Exception, error:
                    # Error wasn't handled so we have nothing else to do.
                    response = self._internal_error(error)

                return response(environ, start_response)


class WebAppWSGIApplication(object):

    def __call__(self, environ, start_response):
        app = get_app()

        request = get_request()
        response = environ['response']

        app.router.dispatch(request, response)

        return response(environ, start_response)


class RenderingContextProcessorsMiddleware(object):

    ENVIRON_KEY = 'templates.rendering_context_processors'

    def __init__(self, app, rendering_context_processors):
        self.app = app
        self.rendering_context_processors = rendering_context_processors

    def __call__(self, environ, start_response):
        environ[self.ENVIRON_KEY] = self.rendering_context_processors
        return self.app(environ, start_response)


class SessionMiddleware(object):

    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        logging.info('Adding session to the request...')

        req = get_request()

        session_store = sessions.get_store(
            factory=self._session_store_factory,
            request=req,
            )
        req.session = session_store.get_session()

        resp = req.get_response(self.app)
        return resp(environ, start_response)
    
    def _session_store_factory(self, request):
        config = {
            'secret_key': request.app.config['secret_key'],
            }
        return SessionStore(request, config)


#{ Rendering context processors


def add_url_utilities_to_rendering_context(request_handler):
    return {
        'relative_url': request_handler.request.relative_url,
        'uri_for': request_handler.uri_for,
        }


#}
