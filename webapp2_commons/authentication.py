"""Back end for the Google Identity Toolkit that integrates with webapp2"""
from functools import wraps
from hashlib import sha256
from urllib import urlencode
from urllib2 import HTTPError
from urlparse import parse_qs
import logging
import urllib2

from google.appengine.ext.db import DateTimeProperty
from google.appengine.ext.db import EmailProperty
from google.appengine.ext.db import LinkProperty
from google.appengine.ext.db import Model
from google.appengine.ext.db import StringProperty
from webapp2 import Route
from webapp2 import get_request
from webapp2 import uri_for
from webapp2_extras import json

from webapp2_commons.common import BaseRequestHandler


__all__ = [
    'add_current_user_to_rendering_context',
    'CurrentUserMiddleware',
    'GoogleIdentityAuthenticationHandler',
    'GoogleIdentityMiddleware',
    'GoogleIDP',
    'FacebookIDP',
    'login_required',
    'User',
    ]


_USER_ID_SESSION_KEY = 'user_id'


#{ Middleware


class GoogleIdentityMiddleware(object):
    """
    Middleware class that sets the settings for Google Identity Toolkit in the
    WSGI environ.
    
    """
    
    def __init__(self, app, google_api_key, custom_providers_config=None):
        self.app = app
        self.google_api_key = google_api_key
        self.custom_providers_config = custom_providers_config
    
    def __call__(self, environ, start_response):
        logging.info('Adding Google Identity settings to the environ...')
        
        environ['google_identity.api_key'] = self.google_api_key
        environ['google_identity.custom_providers'] = \
            self.custom_providers_config
        
        return self.app(environ, start_response)


class CurrentUserMiddleware(object):
    """Middleware class that attaches the current user to the request."""
    
    def __init__(self, app):
        self.app = app
    
    def __call__(self, environ, start_response):
        logging.info('Adding the current request user to the request...')
        
        req = get_request()
        
        user_id = req.session.get(_USER_ID_SESSION_KEY)
        if user_id:
            user = User.get_by_key_name(user_id)
        else:
            user = None
        req.user = user
        
        resp = req.get_response(self.app)
        return resp(environ, start_response)


#{ Decorators


def login_required(handler_method):
    """
    Decorator that ensures the user is logged before a request handler method
    is accessed.
    
    """
    
    @wraps(handler_method)
    def wrapper(self, *args, **kwargs):
        current_user = self.request.user
        if current_user:
            return handler_method(self, *args, **kwargs)
        else:
            self.abort(403)
    
    return wrapper


#{ Request handler


class GoogleIdentityAuthenticationHandler(BaseRequestHandler):
    """
    See https://developers.google.com/identity-toolkit/v1/acguide for
    details.
    
    """
    
    is_extra_data_required = False
    """
    Whether extra data is required after an account for a federated user is
    created.
    
    """
    
    def user_status(self):
        """
        If a user enters an email address in the account chooser, and there is
        no identity provider for that domain, then GITkit will make a HTTP POST
        request to this endpoint to determine if that email address is already
        registered.
        
        GITkit expects a JSON response with at least one boolean parameter.
        
        """
        user_email = self.request.POST['email']
        is_legacy_user = User.get_by_key_name(user_email) is not None
        self.json_response({'registered': is_legacy_user})
    
    def signup(self):
        user_email = self.request.params['email']
        
        if self.request.method == 'POST':
            user = User(
                key_name=user_email,
                email=user_email,
                )
            user.set_password(self.request, self.request.POST['password'])
            user.put()
            
            self.session[_USER_ID_SESSION_KEY] = user_email
            
            self.redirect_to('home')
            
        else:
            self.render_response('signup.html', email=user_email)
    
    def login(self):
        """
        After prompting for a password for a legacy account, GITkit will need to
        know if a password is correct or incorrect.
        
        """
        user_email = self.request.POST['email']
        password = self.request.POST['password']
        
        if User.check_credentials(self.request, user_email, password):
            status = 'OK'
            self.session[_USER_ID_SESSION_KEY] = user_email
        else:
            status = 'passwordError'
        self.json_response({'status': status})
    
    def post_federated_login(self):
        """
        If the user tries to sign in using an identity provider, then the
        identity provider will redirect the user to your site's callback URL.
        
        """
        user_data = self._assert_verification()
        
        user_email = user_data['verifiedEmail']
        
        federated_identity = user_data['identifier']
        
        user = User.get_by_key_name(federated_identity)
        if user is None:
            user = User(
                key_name=federated_identity,
                display_name=user_data.get('displayName'),
                photo_url=user_data.get('photoUrl'),
                email=user_email,
                federated_provider=user_data['authority'],
                federated_identity=federated_identity,
                )
            user.put()
        self.session[_USER_ID_SESSION_KEY] = federated_identity
        
        self.render_response(
            'notify_federated_success.html',
            verified_email=user_email,
            is_user_registered=json.encode(not self.is_extra_data_required),
            )
    
    def _assert_verification(self):
        custom_idp_name = self.request.params.get('rp_custom_idp')
        if custom_idp_name == FacebookIDP.IDP_NAME:
            relative_redirect_url = uri_for(
                'federated_login_callback',
                rp_custom_idp='Facebook',
                rp_purpose='signin',
                )
            idp_config = self.request \
                .environ['google_identity.custom_providers']
            idp = FacebookIDP(
                # Use the same URI that has been used to request the code
                redirect_uri=self.request.relative_url(relative_redirect_url),
                **idp_config[FacebookIDP.IDP_NAME]
                )
            user_data = idp.verify_response(self.request)
        
        elif custom_idp_name:
            raise ValueError(
                u'Custom IDP "{}" is not supported'.format(custom_idp_name),
                )
        
        else:
            api_key = self.request.environ['google_identity.api_key']
            idp = GoogleIDP(api_key)
            user_data = idp.verify_response(self.request)
        
        if not user_data:
            logging.error(
                'Handling of complex federated error cases is not yet '
                'implemented.',
                )
            raise NotImplementedError()
        
        return user_data
    
    def logout(self):
        for key in self.session.keys():
            del self.session[key]
        
        self.redirect_to('home')
    
    @classmethod
    def generate_url_routes(cls):
        return [
            Route(
                '/signup/',
                handler=cls,
                handler_method='signup',
                name='signup',
                ),
            Route(
                '/post-login/',
                handler=cls,
                handler_method='post_federated_login',
                name='federated_login_callback',
                ),
            Route(
                '/login/',
                methods=['POST'],
                handler=cls,
                handler_method='login',
                name='legacy_login',
                ),
            Route(
                '/user-status/',
                methods=['POST'],
                handler=cls,
                handler_method='user_status',
                name='user_status',
                ),
            Route(
                '/logout/',
                methods=['GET'],
                handler=cls,
                handler_method='logout',
                name='logout',
                ),
            ]


#{ Rendering context processors


def add_current_user_to_rendering_context(request_handler):
    user_id = request_handler.session.get(_USER_ID_SESSION_KEY)
    if user_id:
        current_user = User.get_by_key_name(user_id)
    else:
        current_user = None
    
    return {'user': current_user}


#{ Identity providers


class GoogleIDP(object):
    
    VERIFY_URL = 'https://www.googleapis.com/identitytoolkit/v1/relyingparty/verifyAssertion?key='

    def __init__(self, api_key):
        self.api_key = api_key
        
    def verify_response(self, request):
        """
        Sends request to the identity toolkit API end point to verify the IDP
        response.
        
        """
        url = request.url
        logging.debug('Verifying IDP response in %r', url)
        
        data = {
          'requestUri': url,
          'postBody': request.body,
        }
        user_data = self._post_verification(data)
        logging.debug('GITK response: %s', user_data)
        
        if user_data and 'verifiedEmail' in user_data:
            return user_data
        else:
            return None

    def _post_verification(self, data):
        try:
            json_data = json.encode(data)
            cookies = urllib2.HTTPCookieProcessor()
            opener = urllib2.build_opener(cookies)
            request = urllib2.Request(
                url=self.VERIFY_URL + self.api_key,
                headers={'Content-Type': 'application/json'},
                data=json_data,
                )
            response = opener.open(request)
            out = response.read()
            return json.decode(out)
        
        except Exception as error:
            logging.exception(u'Failed to issue the request: %s', error)
            return None


class FacebookIDP(object):
    """
    Custom IDP class that provides authentication services based on the
    authorization controls exposed by Facebook.
    
    See https://developers.facebook.com/docs/authentication/server-side/
    for further details.
    
    """
    
    IDP_NAME = 'Facebook'
    
    AUTHORITY_URL = 'http://www.facebook.com/'
    
    def __init__(self, client_id, client_secret, redirect_uri):
        self.client_id = client_id
        self.client_secret = client_secret
        self.redirect_uri = redirect_uri
    
    def _get_user_access_token(self, code):
        """
        Exchange ``code`` for an User Access Token.
        
        :return: User Access Token and seconds to expire.
        
        """
        user_access_token_url = \
            'https://graph.facebook.com/oauth/access_token?' + \
            urlencode([
                ('client_id', self.client_id),
                ('client_secret', self.client_secret),
                ('redirect_uri', self.redirect_uri),
                ('code', code),
                ])
        logging.debug('Requesting user access token: %r', user_access_token_url)
        
        try:
            raw_response = urllib2.urlopen(user_access_token_url).read()
            
        except HTTPError as error:
            if error.code == 400:
                authz_error = json.decode(error.read())
                logging.error(authz_error)
            raise
        
        server_response = parse_qs(raw_response, strict_parsing=True)
        access_token = server_response['access_token'][0]
        seconds_to_expire = server_response['expires'][0]
        return access_token, seconds_to_expire
    
    def _get_user_basic_data(self, access_token):
        graph_url = 'https://graph.facebook.com/me?access_token=' + access_token
        user_data = urllib2.urlopen(graph_url).read()
        user_data = json.decode(user_data)
        
        logging.debug('Basic info retrieved for FB user: %s', user_data)
        
        photo_url = 'https://graph.facebook.com/{}/picture'.format(
            user_data['username'],
            )
        return {
            'authority': self.AUTHORITY_URL,
            'identifier': self.AUTHORITY_URL + user_data['id'],
            'verifiedEmail': user_data['email'],
            'displayName': user_data['name'],
            'photoUrl': photo_url,
            }
    
    def verify_response(self, request):
        access_token, seconds_to_expire = self._get_user_access_token(
            request.params['code'],
            )
        logging.info(
            'Token %s will expire in %s seconds',
            access_token,
            seconds_to_expire,
            )
        return self._get_user_basic_data(access_token)


#{ User model


class User(Model):
    
    email = EmailProperty(required=True)
    
    federated_provider = StringProperty()
    
    federated_identity = StringProperty()
    
    password = StringProperty()
    
    display_name = StringProperty()
    
    photo_url = LinkProperty()
    
    date_added = DateTimeProperty(auto_now_add=True)
    
    def set_password(self, request, raw_password):
        self.password = self._encode_password(request, raw_password)
    
    @classmethod
    def check_credentials(cls, request, email, password):
        """Check credentials for legacy users."""
        profile = cls.get_by_key_name(email)
        return profile.password == cls._encode_password(request, password)
    
    @classmethod
    def _encode_password(cls, request, string):
        encoded_password = sha256(request.app.config['secret_key'])
        encoded_password.update(string)
        return encoded_password.hexdigest()


#}
